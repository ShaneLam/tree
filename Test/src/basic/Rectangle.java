package basic;

public class Rectangle {
	private int a, b;
	private int Area;
		
	public Rectangle(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	public void sayHello() {
		System.out.println("Ich bin " + this.a + " breit und " + this.b + " lang");	
	}
     
	public double getArea() {
		Area = this.a * this.b;
		return Area;
	}
	
	public void sayArea() {
		System.out.println("Der Flächeninhalt beträgt " + getArea());
	}

	
	//a und b ändern 
	//a und b zurückliefern
	// getCircumference
}
