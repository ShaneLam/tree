package at.lam.tree;

import java.util.List;

public class growing_area {
	private String name;
	private int size;
	private List<tree> trees;
	
	
	public growing_area(String name, int size, List<tree> trees) {
		super();
		this.name = name;
		this.size = size;
		this.trees = trees;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}
	
	public void addtree(tree tree){
		this.trees.add(tree);
	}

	
	
	

}
