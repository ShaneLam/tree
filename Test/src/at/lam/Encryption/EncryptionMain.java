package at.lam.Encryption;

public class EncryptionMain {

	public static void main(String[] args) {
		String encryptedMessage = null;
		String decryptedMessage = null;
		String message = "Shane";
		int key = 4;
		Caesar ceasar = new Caesar();
		Caesar ceasarde = new Caesar();
		encryptedMessage = ceasar.Enrypt(message, key);
		decryptedMessage = ceasarde.Decrypt(encryptedMessage, key);
		
		System.out.println("Encrypted Message = " + encryptedMessage);
		System.out.println("Decrypted Message = " + decryptedMessage);
	}

}
