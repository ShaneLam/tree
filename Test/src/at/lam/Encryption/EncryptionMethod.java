package at.lam.Encryption;

public interface EncryptionMethod {
	
	public String Enrypt(String string, int key);
	public String Decrypt(String string, int key);

}
