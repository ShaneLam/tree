package at.lam.CD;

import java.util.ArrayList;

public class Player{
	
	private ArrayList<Playable> Playables = new ArrayList<Playable>();

	public void playALL() {
		System.out.println("Playing a song now");
		
	}

	public void stopALL() {
		System.out.println("Stopping the song now");
		
	}

	public void pauseALL() {
		System.out.println("Pausing the song now");
		
	}
	
	public void addPlayables(Playable playable) {
		Playables.add(playable);
	}
	
}
