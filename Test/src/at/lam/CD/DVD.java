package at.lam.CD;

public class DVD implements Playable {
	
	private String Name;

	public DVD(String name) {
		super();
		Name = name;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	@Override
	public void play() {
		System.out.println("Playing");
		
	}

	@Override
	public void stop() {
		System.out.println("Stopping");
		
	}

	@Override
	public void pause() {
		System.out.println("Pausing");
		
	}
	

}
