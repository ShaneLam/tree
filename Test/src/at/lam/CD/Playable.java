package at.lam.CD;

public interface Playable {
	
	public void play();
	public void stop();
	public void pause();
	

}
