package at.lam.CD;

public class Song implements Playable{
	
	private String Name;
	private int Length;
	
	public Song(String name, int length) {
		super();
		Name = name;
		Length = length;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getLength() {
		return Length;
	}

	public void setLength(int length) {
		Length = length;
	}

	@Override
	public void play() {
		System.out.println("Playing");
		
	}

	@Override
	public void stop() {
		System.out.println("Stoping");
		
	}

	@Override
	public void pause() {
		System.out.println("Pausing");
		
	}
	
	
}
