package at.lam.CD;

import java.util.ArrayList;

public class Actor {
	
	private String Firstname;
	private String Fastname;
	private ArrayList<CD> CDList = new ArrayList<CD>();
	
	public Actor(String firstname, String fastname) {
		super();
		Firstname = firstname;
		Fastname = fastname;
	}

	public String getFirstname() {
		return Firstname;
	}

	public void setFirstname(String firstname) {
		Firstname = firstname;
	}

	public String getFastname() {
		return Fastname;
	}

	public void setFastname(String fastname) {
		Fastname = fastname;
	}
	
	public void addCD(CD cd) {
		CDList.add(cd);
	}
	
	public void getAllRecords() {
		for (CD cd : CDList) {
			cd.getName();
		}
	}

}
