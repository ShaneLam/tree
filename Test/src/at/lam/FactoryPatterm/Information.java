package at.lam.FactoryPatterm;

public class Information {
	
	private static Information InformationText = new Information();
	
	private Information() {
		System.out.println("This is InformationText");
	}
	
	public static Information getInformation() {
		return InformationText;
	}

}
