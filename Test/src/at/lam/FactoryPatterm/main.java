package at.lam.FactoryPatterm;

public class main {

	public static void main(String[] args) {
		
		Shape s1 = ShapeFactory.CreateShape("Circle");
		s1.SayType();
		
		Shape s2 = ShapeFactory.CreateShape("Rectangle");
		s2.SayType();
		
		Information.getInformation();
	
	}

}
