package at.lam.ObserverPattern;

public class main {

	public static void main(String[] args) {
		
		Sensor sensor = new Sensor();
		Lantern l1 = new Lantern();
		Lantern l2 = new Lantern();
		ChristmasTree t1 = new ChristmasTree();
		
		sensor.addObserveables(l1);
		sensor.addObserveables(l2);
		sensor.addObserveables(t1);
		sensor.informAll();
	}

}
