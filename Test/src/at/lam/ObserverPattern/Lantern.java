package at.lam.ObserverPattern;

public class Lantern implements Observeable {

	@Override
	public void inform() {
		System.out.println("I am informed");	
	}
	

}
