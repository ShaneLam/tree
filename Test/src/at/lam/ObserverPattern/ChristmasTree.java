package at.lam.ObserverPattern;

public class ChristmasTree implements Observeable {

	@Override
	public void inform() {
		System.out.println("I am informed");	
		
	}

}
