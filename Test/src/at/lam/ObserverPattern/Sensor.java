package at.lam.ObserverPattern;

import java.util.ArrayList;

public class Sensor {
	
	private ArrayList<Observeable> Oberveables = new ArrayList<Observeable>();
	
	
	public void addObserveables(Observeable ob) {
		Oberveables.add(ob);
	}
	
	public void informAll() {
		for(Observeable ob : this.Oberveables) {
			System.out.println("I am informed");
		}
	}
}
