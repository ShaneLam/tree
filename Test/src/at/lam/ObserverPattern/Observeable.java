package at.lam.ObserverPattern;

public interface Observeable {
	
	public void inform();

}
