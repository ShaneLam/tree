package at.lam.TicTacToe;

public class TTTObject {

	private char[][] arrField = new char[3][3];
	private char PlayerMark;

	public char getPlayerMark() {
		return PlayerMark;
	}

	public void setPlayerMark(char playerMark) {
		PlayerMark = playerMark;
	}

	public TTTObject() {

	}

	public void clearBoard() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				arrField[i][j] = '-';
			}
		}
	}

	public void createBoard() {
		System.out.println("--------------");

		for (int i = 0; i < 3; i++) {
			System.out.print(" | ");
			for (int j = 0; j < 3; j++) {
				System.out.print(arrField[i][j] + " | ");
			}
			System.out.println();
			System.out.println("--------------");
		}
	}

	public void changePlayer() {
		if (PlayerMark == 'x') {
			PlayerMark = 'o';
		} else {
			PlayerMark = 'x';
		}
	}

	public boolean Mark(int row, int col) {
		if ((row >= 0) && (row < 3)) {
			if ((col >= 0) && (row < 3)) {
				if (arrField[row][col] == '-') {
					arrField[row][col] = PlayerMark;
					return true;
				}
			}
		}

		return false;
	}

	public Boolean checkwinner() {
		return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
	}

	private boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}

	public boolean checkRowsForWin() {

		for (int i = 0; i < 3; i++) {
			if (checkRowCol(arrField[i][0], arrField[i][1], arrField[i][2]) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkColumnsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(arrField[0][i], arrField[1][i], arrField[2][i]) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkDiagonalsForWin() {
		return ((checkRowCol(arrField[0][0], arrField[1][1], arrField[2][2]) == true)
				|| (checkRowCol(arrField[0][2], arrField[1][1], arrField[2][0]) == true));
	}

	public Boolean checkfield() {
		boolean full = true;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (arrField[i][j] == '-')
					full = false;
			}
		}
		return true;
	}

	public Boolean checkplayer() {

		return true;
	}
}
