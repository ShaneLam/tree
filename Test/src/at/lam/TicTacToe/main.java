package at.lam.TicTacToe;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		TTTObject game = new TTTObject();
		Scanner scanner = new Scanner(System.in);
		game.clearBoard();
		do
		{
			game.createBoard();
			int row;
			int col;
			do
			{
				System.out.println("Please enter the field u want to mark ");
				row = scanner.nextInt()-1;
				col = scanner.nextInt()-1;
			}
			while (!game.Mark(row, col));
			game.changePlayer();
		}
		while(!game.checkwinner() && !game.checkfield());
		game.createBoard();
		game.changePlayer();

	}

}
