package at.lam.Algorithm;

public interface Algorithm {
	
	public int doSort(int[] array);

}
