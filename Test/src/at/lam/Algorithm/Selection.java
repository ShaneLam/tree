package at.lam.Algorithm;

public class Selection implements Algorithm {

	@Override
	public int doSort(int[] array) {
		
		 for (int i = 0; i < array.length - 1; i++) 
		 {
			 int min = i;  
	            for (int j = i + 1; j < array.length; j++){  
	                if (array[j] < array[min]){  
	                    min = j;//searching for lowest index  
	                }  
	            }  
	            int temp = array[min];   
	            array[min] = array[i];  
	            array[i] = temp; 
			 
		 }
		
		return 0;
	}

}
