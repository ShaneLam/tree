package at.lam.Algorithm;

import java.util.Random;

public class main {
	

	public static void main(String[] args) {
		
		Random rd = new Random(); // creating Random object
	      int[] arr = new int[10];
	      for (int i = 0; i < arr.length; i++) {
	         arr[i] = rd.nextInt(); // storing random integers in an array
	      }
	      
		Algorithm b1 = new Bubble();
		Algorithm s1 = new Selection();
		
		long starttime = System.currentTimeMillis();
		
		
		s1.doSort(arr);
		long endtime = System.currentTimeMillis()-starttime;
		

		
		for(int i=0; i < arr.length; i++){  
             System.out.println(arr[i] + " ");  
		 }  
		 
		System.out.println(endtime/1000f);
		 
	}

}
